import os
import sys
import glob as glob
import numpy as np
from astropy import units as u
# tried quantity_support to plot quantities, but doesn't work for plt.errorbar
#from astropy.visualization import quantity_support
from matplotlib import pyplot as plt


def cat_tim(timin, timout):
    incfile = np.array([],dtype='string')
    tim = open(timin,'r')
    for t in tim.readlines():
        if t.startswith('INCLUDE'):
            incfile = np.append(incfile, t)
    cmd = 'cat '
    for i in incfile:
        i = i.strip('\n').strip('INCLUDE').strip(' ')
        cmd += '%s ' %i
    cmd += '> cat.tim'
    print(cmd)
    os.system(cmd)
    tim.close()
    cattim = open('cat.tim','r')
    newcattim = open('newcat.tim','w')
    newcattim.write('MODE 1\nFORMAT 1\n')
    for l in cattim.readlines():
        if 'MODE' in l:
            l = 'C %s' % l
            newcattim.write(l)
        elif 'FORMAT' in l:
            l = 'C %s' % l
            newcattim.write(l)
        else:
            newcattim.write(l)
    newcattim.close()
    cattim.close()
    os.system('mv newcat.tim %s' % timout)
    os.system('rm cat.tim')


def calc_rms(npulsefile):
    resids = np.loadtxt(npulsefile, usecols=(0,), unpack=True, dtype=float)
    rms = np.sqrt(np.mean(resids**2))
    return rms

    
def write_pulsenumbers(npulsefile, tim, tim_pn):
    npulse=np.loadtxt(npulsefile,usecols=(1,),unpack=True,dtype=str)
    #print('Number of pulses = %d' % len(npulse))
    timin = open(tim, 'r')
    timout = open(tim_pn, 'w')
    timout.write('MODE 1\nFORMAT 1\n')
    counter = 0
    for l in timin.readlines():
        if ('-group' in l):
            if not l.strip().startswith('C') and not l.strip().startswith('#'):
                timout.write('%s  %s\n' % (l.strip(), npulse[counter]))
                counter = counter+1
        if l.startswith('TIME '):
            timout.write('%s' % l)
    timin.close()
    timout.close()


def check_binary(parfile):
    par = open(parfile, 'r')
    lines = par.readlines()
    par.close()
    for line in lines:
        if line.startswith('BINARY'):
            return True
        
    
def correct_parfile_lines(parfile, newpar, binary):
    px = False
    out = open(newpar, 'w')
    par = open(parfile, 'r')
    lines = par.readlines()
    par.close()
    
    # Get relevant values
    for line in lines:
        if line.startswith('START'):
            start = float(line.split()[1])
        elif line.startswith('FINISH'):
            finish = float(line.split()[1])
        elif line.startswith('F0'):
            f0 = float(line.split()[1])
        elif line.startswith('F1'):
            f1 = float(line.split()[1])
        elif line.startswith('PEPOCH'):
            orig_epoch = float(line.split()[1])
        elif line.startswith('T0') or line.startswith('TASC'):
            torb = float(line.split()[1])
            tbin = True
        elif line.startswith('PB '):
            pb = float(line.split()[1])
    
    # Calculate midpoint of data span, F0 at midpoint, and T0/TASC closest to the midpoint
    tmid = (start+finish)/2.0
    f0_mid = f0 + (tmid-orig_epoch)*f1
    print('F0 at midpoint = %.12f' % f0_mid)
    if binary:
        norbits = int((tmid-torb)/pb)
        torb_mid = torb + norbits*pb

    # Correct relevant lines
    for line in lines:
        #
        # Correct F0 for PEPOCH at midpoint; not using this right now
        #if line.startswith('F0'):
        #    out.write('F0      %.20f   1\n' % f0_mid)
        #
        # Remove '1' from START and FINISH lines (i.e., don't fit these)
        if line.startswith('START') or line.startswith('FINISH'):
            if len(line.split()) > 2:
                newline = line.strip().strip('1').strip()
                out.write('%s\n' % newline)
            else:
                out.write(line)
                continue
        #
        # Make sure PX is being fit for if it's in the file
        elif line.startswith('PX'):
            px = True
            if line.split()[2] is '1':
                out.write(line)
                continue
            else:
                out.write('PX             %s     1\n' % line.split())
        #
        # If a binary, correct T0 or TASC to be near middle of data span
        elif line.startswith('T0') or line.startswith('TASC'):
            out.write('%s            %.8f     1\n' % (line.split()[0],torb_mid))
        #
        # Change reference epoch to be in middle of data span
        # Not using this right now.  Might be better not to change PEPOCH.  Want to have
        #   PEPOCH coincide with a time when TOAs exist.
        #elif line.startswith('PEPOCH') or line.startswith('POSEPOCH') or line.startswith('DMEPOCH'):
        #    out.write('%s            %.5f\n' % (line.split()[0], tmid))
        else:
            out.write(line)

    # If PX was not found in the parfile, then put it into the parfile. Not using this right now.
    #if not px:
    #    out.write('PX             0.0     1\n')
        
    out.close()

    if binary:
        return torb, torb_mid, pb
    

def replace_torb(parfile, torb_new):
    out = open('tmp.par', 'w')
    par = open(parfile, 'r')
    lines = par.readlines()
    par.close()
    for line in lines:
        if line.startswith('T0') or line.startswith('TASC'):
            out.write('%s            %.8f     1\n' % (line.split()[0],torb_new))
        else:
            out.write(line)
    out.close()
    os.system('mv tmp.par %s' % parfile)


def get_binary_params(parfile):
    par = open(parfile, 'r')
    lines = par.readlines()
    par.close()
    for line in lines:
        if line.startswith('T0') or line.startswith('TASC'):
            torb = float(line.split()[1])
            tbin = True
        elif line.startswith('PB '):
            pb = float(line.split()[1])
    return torb, pb


######## Main body of script ########

#release_dir = '/Users/decesame/Megan/IPTA/Data_Releases/DR2/megan_testing/Testing_Final/my_release'

help = """\nThis script corrects the following in IPTA parfiles:\n
     1. Removes '1' from START and FINISH lines if it's there
     2. Moves T0 or TASC to center of data span
     3. Moves POSEPOCH, DMEPOCH, and PEPOCH  (not currently implemented)
     4. Includes and frees PX (not currently implemented)
To run on a single pulsar:\n
     python make_parfile_corrections.py <release_dir> <pulsar name>
e.g.,
     python make_parfile_corrections.py /home/DR2/my_release J0437-4715\n
To run on multiple pulsars:\n
     python make_parfile_corrections.py <release_dir> <pulsar1>,<pulsar2>
e.g.,
     python make_parfile_corrections.py J0437-4715,B1937+21\n
The pulsar name must start with "J" or "B" and be the same as that pulsar's directory name in the release dir.
To run on all pulsars in the release directory:\n
     python make_parfile_corrections.py <release_dir> all\n
Note: If a fit will not converge, run 'tempo2 -gr plk' on the relevant par and tim file \
to see what the residuals look like.  If there are one or a few outliers, comment out \
that TOA(s), run this script, replace the TOA(s) after this script has finished, and check \
with 'tempo2 -gr plk' that the residuals still look good once the TOA(s) is back in the \
timfile.   As an example, I needed to remove this TOA in order to get the fit to work \
for J1643-1224:\n
c034859.align.pazr.30min 2628.299 54834.3945602171476 2.918 g   -pta EPTA -group EFF.EBPP.2639 -sys EFF.EBPP.2639 -bw 89.518 -tobs 1801.079 -padd 0.505192\n
I uncommented it afterward, and checked with 'tempo2 -gr plk' that J1643 showed flat residuals when this TOA was included.\n
If you want to see any or all of the output files, then comment one or more of the \
'os.unlink' lines at the end of the script.  This can be useful for troubleshooting; for \
example, you can comment the 'os.unlink(pulsetimfile)' line and then see what the residuals \
look like when the pulse numbers are included, using 'tempo2 -gr plk <parfile> <psrname>.IPTADR2v1.pulses.tim'.
  """

if len(sys.argv) < 3:
    print(help)
    sys.exit()
elif len(sys.argv) == 3:
    release_dir = sys.argv[1].strip('/')
    if sys.argv[2] == 'all':
        psrs = sorted(glob.glob('%s/*' % release_dir))
        for i in range(0,len(psrs)):
            psrs[i] = psrs[i].split('/')[-1]
    elif ',' in sys.argv[2]:
        psrs = sys.argv[2].strip().split(',')
    else:
        if sys.argv[2].startswith('J') or sys.argv[2].startswith('B'):
            psrs = np.array([sys.argv[2].strip()])
        else:
            print(help)
            sys.exit()
else:
    print(help)
    sys.exit()


os.chdir(release_dir)

for i in range(0, len(psrs)):
    psr = psrs[i]
    os.chdir(psr)
    parfile = '%s.IPTADR2v1.par' % psr
    timfile = '%s.IPTADR2v1.tim' % psr
    intermpar = '%s.IPTADR2v1.interm.par' % psr
    correctedpar = '%s.IPTADR2v1.centerT0.par' % psr
    cattim = '%s.IPTADR2v1.catall.tim' % psr
    npulsefile = '%s_pulses.dat' % psr
    timpulsefile = '%s.IPTADR2v1.pulses.tim' % psr
    residsfile = '%s_resids.dat' % psr
    residsplot = '%s_resids.png' % psr

    print('\n--- %s ---\n' % psr)

    print('\nConcatenating tims into single timfile.\n')
    cat_tim(timfile, cattim)

    print('\nRunning tempo2 to get pulse numbers.\n')
    os.system('tempo2 -output general2 -outfile %s -f %s %s -s "{post} {npulse}\n"' % (npulsefile, parfile, timfile))
    prefit_rms = calc_rms(npulsefile)
    write_pulsenumbers(npulsefile, cattim, timpulsefile)

    binary = False
    binary = check_binary(parfile)

    print('\nCorrecting parfile lines: removing "1" in START and FINISH lines, and replacing T0 or TASC with MJD near center of data span.\n')
    if binary:
        (torb, torb_mid, pb) = correct_parfile_lines(parfile, intermpar, binary)
    else:
        correct_parfile_lines(parfile, intermpar, binary)

    print('\nRunning tempo2 with corrected parfile, and timfile with pulse numbers, to fit updated parameters.\n') # and with new PEPOCH.\n')
    os.system('tempo2 -output general2 -outfile postfit.dat -f %s %s -s "{post}\n"' % (intermpar, timpulsefile))

    postfit_rms = calc_rms('postfit.dat')
    print(prefit_rms, postfit_rms, postfit_rms/prefit_rms)

    if binary:
    
        torb_orig = torb
        torb_mid_orig = torb_mid

        if postfit_rms/prefit_rms > 1.1:
            print('\nPostfit RMS too large, searching for t_orb closer to original value.\n')
            n_pb = int((torb_mid-torb)/pb)
            torb_tmp = torb
            torb_mid_tmp = torb_mid
            while postfit_rms/prefit_rms > 1.1:
                if torb_mid_tmp > torb:
                    torb_new = torb + pb * int((torb_mid_tmp-torb)/pb/2.)
                else:
                    torb_new = torb - pb * int((torb_mid_tmp-torb)/pb/2.)
                replace_torb(intermpar,torb_new)
                os.system('tempo2 -output general2 -outfile postfit.dat -f %s %s -s "{post}\n"' % (intermpar, timpulsefile))
                postfit_rms = calc_rms('postfit.dat')
                torb_mid_tmp = torb_new
                print(prefit_rms, postfit_rms, postfit_rms/prefit_rms)
                    
            n_pb_step = int((torb_new-torb)/pb)

            print('\nFound good torb of %.4f.\n' % torb_new)
            print('\nOverwriting %s.IPTADR2v1.interm.par with the fit values corresponding to the new torb=%.4f.\n' % (psr, torb_new))
            os.system('tempo2 -newpar -f %s %s' % (intermpar, timpulsefile))
            os.system('mv new.par %s' % intermpar)
            print('Grabbing updated values of torb and pb from new parfile.\n')
            torb, pb = get_binary_params(intermpar)

            print('\nNow moving torb toward torb_mid, %.4f.\n' % torb_mid_orig)
            
            while torb_new <= torb_mid_orig:
                torb_new = torb+pb*n_pb_step
                replace_torb(intermpar, torb_new)
                os.system('tempo2 -output general2 -outfile postfit.dat -f %s %s -s "{post}\n"' % (intermpar, timpulsefile))
                postfit_rms = calc_rms('postfit.dat')
                print(prefit_rms, postfit_rms, postfit_rms/prefit_rms)
                if postfit_rms/prefit_rms <= 1.1:
                    os.system('tempo2 -newpar -f %s %s' % (intermpar, timpulsefile))
                    os.system('mv new.par %s' % intermpar)
                    torb, pb = get_binary_params(intermpar)
                else:
                    print('Postfit RMS too large at torb=%.4f; reducing step size.\n' % torb_new)
                    i=2
                    j=1
                    while postfit_rms/prefit_rms > 1.1:
                        if n_pb_step/i >= 5:
                            torb_new = torb+pb*n_pb_step/i
                            print('\nN_pb_step/%d = %d\n' % (i, n_pb_step/i))
                            i=i+1
                        else:
                            torb_new = torb+pb*n_pb_step/i-j
                            print('\nN_pb_step/%d - %d = %d\n' % (i, j, n_pb_step/i-j))
                            j=j+1
                        replace_torb(intermpar, torb_new)
                        os.system('tempo2 -output general2 -outfile postfit.dat -f %s %s -s "{post}\n"' % (intermpar, timpulsefile))
                        postfit_rms = calc_rms('postfit.dat')
                        print(prefit_rms, postfit_rms, postfit_rms/prefit_rms)
                    os.system('tempo2 -newpar -f %s %s' % (intermpar, timpulsefile))
                    os.system('mv new.par %s' % intermpar)
                    torb, pb = get_binary_params(intermpar)


    os.system('tempo2 -newpar -f %s %s' % (intermpar, timpulsefile)) # timfile))
    os.system('mv new.par %s' % correctedpar)

    print('\nRunning tempo2 with original .tim file and plotting to check that residuals are flat.\n')
    os.system('tempo2 -output general2 -outfile %s -f %s %s -s "{sat} {freqSSB} {post} {err}\n"' % (residsfile, correctedpar, timfile))
    #os.system('tempo2 -gr plk -f %s %s' % (correctedpar, timfile))

    mjd, resid, err = np.loadtxt('%s_resids.dat'%psr, dtype=float, unpack=True, usecols=(0,2,3))
    resid = resid*u.s
    resid_us = resid.to(u.us)
    err = err*u.us
    # Can't plot quantities so need to grab values for plotting
    resid_us_val = np.zeros(len(resid_us), dtype=float)
    err_val = np.zeros(len(err), dtype=float)
    for i in range(0,len(resid)):
        resid_us_val[i] = resid_us[i].value
        err_val[i] = err[i].value
    plt.errorbar(mjd, resid_us_val, yerr=err_val, linestyle='None')
    plt.title('%s' % psr)
    plt.xlabel('MJD')
    plt.ylabel('Residuals (us)')
    plt.savefig(residsplot)
    plt.clf()

    os.unlink(intermpar)
    os.unlink(cattim)
    os.unlink(npulsefile)
    os.unlink(residsfile)
    os.unlink(timpulsefile)
    os.unlink('postfit.dat')
    os.chdir('..') # or os.chdir(release_dir)
