import os, sys, glob
from shutil import copyfile

if len(sys.argv)==1:
    print "usage:  python utils/assemble_tims.py PSRJNAME"
    sys.exit(0)

# make sure we are in the top-level 
curdir = os.path.split(os.path.abspath('.'))[-1]
if curdir=="utils":
    os.chdir("..")

jpsr = sys.argv[1]
bpsrs = {"J1857+0943": "B1855+09",
         "J1939+2134": "B1937+21", 
         "J1955+2908": "B1953+29"}
psr = bpsrs[jpsr] if jpsr in bpsrs.keys() else jpsr

curdir = os.path.abspath('.')
targetdir = os.path.join(curdir, "pulsars", jpsr)
timsdir = os.path.join(targetdir, "tims")

try:
    os.mkdir(targetdir)
except:
    pass

try:
    os.mkdir(timsdir)
except:
    pass

timstr = "FORMAT 1\n"

# Copy NANOGrav file
fname = psr+"_NANOGrav_9yv1.tim"
dest = os.path.join(timsdir, fname)
timstr += "INCLUDE tims/%s\n" % fname
try:
    copyfile(os.path.join(curdir, "NANOGrav_9y/tim", 
                          psr+"_NANOGrav_9yv1.tim"), dest)
except:
    pass

# Copy EPTA files
tfiles = glob.glob("EPTA_v2.2/%s/tims/*tim" % jpsr)
for tfile in tfiles:
    fname = os.path.split(tfile)[-1]
    dest = os.path.join(timsdir, fname)
    timstr += "INCLUDE tims/%s\n" % fname
    try:
        copyfile(os.path.join(curdir, tfile), dest)
    except:
        pass

# Copy PPTA file(s)
tfiles = glob.glob("PPTA_dr1dr2/tim/v3/%s_*tim" % jpsr)
for tfile in tfiles:
    fname = os.path.split(tfile)[-1]
    dest = os.path.join(timsdir, fname)
    timstr += "INCLUDE tims/%s\n" % fname
    try:
        copyfile(os.path.join(curdir, tfile), dest)
    except:
        pass

# Write the top-level tim file
outf = open(os.path.join(targetdir, jpsr+".tim"), "w")
outf.write(timstr)
outf.close()
