# NG9(-PPTA4) + PPTA4

This contains the 15 of the pulsars used in the NG 9 year stochastic paper along 
with PPTA data used in the Shannon et al (2016) paper for PSRS J1713+0747, J1909-3744, J1744-1134, and J0437-4715

## Specifications

* All data is filtered using a fractional bandwidth of 1.1 (i.e. nu_max/nu_min >= 1.1) with the exception that 
PPTA 10 cm data is kept.
* All JUMPS corresponding to dropped data are removed.
